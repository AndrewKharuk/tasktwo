﻿using System;

namespace TaskTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            ScrumTask taskOne = new ScrumTask("One", "Text text text", 10 );

            ScrumTask taskTwo = new ScrumTask("Two", "O o o o o o", 25);

            ScrumTask taskThree;

            if (taskOne < taskTwo)
            {
                taskThree = taskOne + taskTwo;
                taskThree.ReadToConsole();
            }

            string newName = "Four";
            ScrumTask taskFour = newName.GetScrumTask(11);
            taskFour.ReadToConsole();

            Console.ReadKey();
        }
    }
}
