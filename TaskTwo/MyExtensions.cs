﻿namespace TaskTwo
{
    public static class MyExtensions
    {
        public static ScrumTask GetScrumTask(this string name, int duration)
        {
            return new ScrumTask(name, null, duration);
        }
    }
}
