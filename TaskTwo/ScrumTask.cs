﻿using System;

namespace TaskTwo
{
    /// <summary>
    /// Задача
    /// </summary>
    public class ScrumTask : IComparable<ScrumTask>
    {
        /// <summary>
        /// Наименование задачи
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Содержание задачи
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Плановое время выполнения задачи в условных единицах
        /// </summary>
        public int  Duration { get; set; }

        public ScrumTask()
        {
        }

        public ScrumTask(string name, string text, int duration)
        {
            Name = name;
            Text = text;
            Duration = duration;
        }

        /// <summary>
        /// Вывод информации о Задаче на консоль
        /// </summary>
        public void ReadToConsole()
        {
            Console.WriteLine(this.ToString());
            Console.WriteLine($"Text: {Text}");
        }

        public override string ToString()
        {
            return $"Task {Name}: Duration {Duration}";
        }

        public override bool Equals(object obj)
        {
            return (obj as ScrumTask).Duration == this.Duration;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public int CompareTo(ScrumTask other)
        {
            if (this.Duration > other.Duration)
                return 1;
            if (this.Duration < other.Duration)
                return -1;
            else
                return 0;
        }

        public static ScrumTask operator + (ScrumTask t1, ScrumTask t2)
        {
            return new ScrumTask { Name = t1.Name + ". " + t2.Name, Duration = t1.Duration + t2.Duration, Text = t1.Text + ". " + t2.Text };
        }

        public static bool operator ==(ScrumTask t1, ScrumTask t2)
        {
            return t1.Equals(t2);
        }

        public static bool operator !=(ScrumTask t1, ScrumTask t2)
        {
            return !t1.Equals(t2);
        }

        public static bool operator >(ScrumTask t1, ScrumTask t2)
        {
            return (t1.CompareTo(t2) > 0);
        }

        public static bool operator <(ScrumTask t1, ScrumTask t2)
        {
            return (t1.CompareTo(t2) < 0);
        }

        public static bool operator >=(ScrumTask t1, ScrumTask t2)
        {
            return (t1.CompareTo(t2) >= 0);
        }

        public static bool operator <=(ScrumTask t1, ScrumTask t2)
        {
            return (t1.CompareTo(t2) <= 0);
        }
    }
}
